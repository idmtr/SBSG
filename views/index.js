import "../assets/scss/main.scss";

import "../assets/css/fontawesome.css";

import '../node_modules/jquery/src/jquery.js';

import $ from 'jquery';
window.jQuery = $;
window.$ = $;

// Good
import 'popper.js/dist/popper.min.js';

import '../node_modules/bootstrap/dist/js/bootstrap.js';

import '../assets/js/main.js';
