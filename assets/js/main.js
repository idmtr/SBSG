$(window).scroll(function() {
    var nav = $('#navStartupBerlin');
    if ($(window).scrollTop() >= 5) {

        nav.removeClass('navbar-dark');
        nav.find('.logo-white').hide();
        nav.find('.logo-color').show();
        nav.addClass('navbar-light bg-light shadow-lg');
        nav.css({'margin-top': '0'});

    } else {
        nav.removeClass('navbar-light bg-light shadow-lg');
        nav.find('.logo-color').hide();
        nav.find('.logo-white').show();
        nav.addClass('navbar-dark');
        nav.css({'margin-top': '94px'});
    }
});

$(function() {
  $('body').addClass("sb-animate");
  $('[data-toggle="tooltip"]').tooltip();
});

import renderDirectory from '../../views/home/directory-item.pug'

$(async function() {

    function get(...args) {
      return new Promise((resolve, reject) => {
        $.get(...args)
          .done(resolve)
          .fail(reject);
      })
    }

    function decodeHtmlEntities(html) {
      const elem = document.createElement('div');
      elem.innerHTML = html;
      return elem.innerText;
    }

    const SIZES = ['medium', 'thumbnail', 'chipmunk-sm'];
    const data = await get("https://directory.startupberlin.co/wp-json/wp/v2/resources");
    // console.log(data);
    const $directoryDiv = $('#directory-entries')
    const $directoryTitle = $directoryDiv.find('.card-sb__title');
    const $directoryDescription = $directoryDiv.find('.card-sb__description-p');
    const $directoryLink = $directoryDiv.find('.card-sb__link');

    $directoryDiv.empty();

    data.slice(0, 6).forEach(async item => {
      const title = decodeHtmlEntities(item.title.rendered);
      const description = decodeHtmlEntities(item.excerpt.rendered);
      const link = item.link;

      let img;

      let featuredMedia;
      try {
          featuredMedia = item._links['wp:featuredmedia'][0].href;
      } catch (e) {}

      if (featuredMedia) {
        const fmData = await get(featuredMedia);
        const medias = fmData.media_details.sizes;

        for (const size of SIZES) {
            if (medias[size] && medias[size].source_url) {
                img = medias[size].source_url;
                break;
            }
        }
      }

      //+cardDirectory({img, title, tags, description, link})

      const html = renderDirectory({ title, img, link, description });

      const elem = document.createElement('div');
      elem.className = 'col-md-4 mb-5 mb-sm-0';
      elem.innerHTML = html;

      $directoryDiv.append(elem);
    });
});


(function() {
  'use strict';
  // elements
  const changeFormat = document.getElementById('changeFormat'),
        currentTemp = document.getElementById('temperature');


  // get location
  $.ajax({
    url: "https://geoip-db.com/jsonp/95.91.213.134",
    jsonpCallback: "callback",
    dataType: "jsonp",
    success: function(pos) {
      // location
      let city = pos.city;
      let state = pos.state;
      state = state.split(' ').length > 1 ? state.split(' ').map((word) => word[0].toUpperCase()).join('') : state;
      let postCode = pos.postal;
      let locationString = [city, state, postCode].join(' ');
      location.textContent = locationString;
      getWeather(pos);
    }
  });

  // get weather data
  function getWeather(pos) {
    let latitude = pos.latitude;
    let longitude = pos.longitude;
    let url = `https://api.darksky.net/forecast/24dfbe35483e5954d6da5665c468a40f/${latitude},${longitude}?units=ca`;
    $.ajax({
      type: 'GET',
      url: url,
      dataType: 'jsonp',
      success: function(data) {
        // weather = data;
        console.log(data);
        // today's icon
        // future icons
        let days = data.daily.data;
        for (let i = 0, len = days.length; i < len; ++i) {
        }
        // load the weather
        loadWeather(data);
      }
    });
  }

  function loadWeather(data) {
    // current weather information
    let c = data.currently;
    
    // temperature
    loadTemperature(data);
  }
  
  function loadTemperature(data) {
    let today = data.currently;
    // load today's temperature
    currentTemp.textContent = Math.round(today.temperature);
   }
 })();
