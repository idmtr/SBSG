const Airtable = require('airtable');
// const { API_KEY, BASE_ID, TABLE_NAME } = process.env;  // Use environment variables for API Key and Base ID
const API_KEY = 'pather5TbwcXeMHw3.46a6ec72bfd3670258f37efe05d6a9f580ee793dd3174d2575cfce69e2c63265';
const BASE_ID = 'appQ4nReiJhbgxsNL';
const TABLE_NAME = 'Jobs';

const base = new Airtable({ apiKey: API_KEY }).base(BASE_ID);

exports.handler = async (event, context) => {
    console.log('Received event:', event);

    if (event.httpMethod === 'POST') {
        let body;
        try {
            body = JSON.parse(event.body);
            console.log('Parsed body:', body);
        } catch (error) {
            console.error('Error parsing event body:', error);
            return {
                statusCode: 400,
                body: JSON.stringify({ message: 'Invalid event body.' }),
            };
        }

        // Handle Slack's URL verification challenge
        if (body.type === 'url_verification') {
            console.log('Responding to URL verification challenge.');
            return {
                statusCode: 200,
                body: body.challenge,
            };
        }

        if (body.event) {
            console.log('Processing event:', body.event);

            // Ignore certain subtypes if necessary (e.g., channel_join)
            if (body.event.subtype && body.event.subtype !== 'bot_message') {
                console.log(`Ignoring event with subtype: ${body.event.subtype}`);
                return {
                    statusCode: 200,
                    body: JSON.stringify({ message: `Ignored event with subtype: ${body.event.subtype}` }),
                };
            }

            if (body.event.channel === "C0FD5CYSD") {
                console.log('Event is from the monitored channel.');

                const messageId = body.event.ts;
                const messageContent = body.event.text || (body.event.message && body.event.message.text);
                const userName = body.event.user_profile ? body.event.user_profile.display_name || body.event.user_profile.real_name : 'Unknown User';
                const emailRegex = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/i;
                const emailMatch = messageContent.match(emailRegex);
                const contactEmail = emailMatch ? emailMatch[0] : 'No email found';
                const links = parseLinksFromBlocks(body.event.blocks);

                if (!messageContent) {
                    console.log('No message content found.');
                    return {
                        statusCode: 200,
                        body: JSON.stringify({ message: 'No message content found.' }),
                    };
                }

                console.log('Message content:', messageContent);

                // Save the Slack message to Airtable
                try {
                    console.log('Saving to Airtable...');
                    const record = await base(TABLE_NAME).create([
                        {
                            fields: {
                                'Message ID': messageId,
                                'Message': messageContent,
                                'Author': userName,
                                'Email': contactEmail,
                                'Links': links.join(', '),
                                'Tags': [], // Assuming tags are not needed for simplicity
                            }
                        }
                    ]);
                    console.log('Record saved:', record);

                    return {
                        statusCode: 200,
                        body: JSON.stringify({ message: 'Slack message saved to Airtable!' }),
                    };
                } catch (error) {
                    console.error('Error saving message to Airtable:', error.message, 'Code:', error.code);
                    return {
                        statusCode: 500,
                        body: JSON.stringify({ message: 'Error saving message to Airtable.', error: error.message }),
                    };
                }
            } else {
                console.log('Event not from the monitored channel.');
            }
        } else {
            console.log('No event found in the body.');
        }

        return {
            statusCode: 200,
            body: JSON.stringify({ message: 'Event not processed.' }),
        };
    }

    return {
        statusCode: 405,
        body: JSON.stringify({ message: 'Method Not Allowed' }),
    };
};

// Helper function for parsing links from the blocks section
function parseLinksFromBlocks(blocks) {
    const links = [];
    if (!blocks || !Array.isArray(blocks)) {
        console.log('No blocks found or blocks is not an array.');
        return links;
    }

    blocks.forEach(block => {
        if (block.type === 'rich_text' && block.elements && Array.isArray(block.elements)) {
            block.elements.forEach(element => {
                if (element.elements && Array.isArray(element.elements)) {
                    element.elements.forEach(subElement => {
                        if (subElement.type === 'link') {
                            links.push(subElement.url);
                        }
                    });
                }
            });
        }
    });

    return links;
}
