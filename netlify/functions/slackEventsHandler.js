const Airtable = require('airtable');

// Airtable API key and base configuration
const API_KEY = 'pather5TbwcXeMHw3.46a6ec72bfd3670258f37efe05d6a9f580ee793dd3174d2575cfce69e2c63265';
const BASE_ID = 'appQ4nReiJhbgxsNL';
const TABLE_NAME = 'Events';

const base = new Airtable({ apiKey: API_KEY }).base(BASE_ID);

exports.handler = async (event, context) => {
    console.log('Received event:', event);

    if (event.httpMethod === 'POST') {
        let body;
        try {
            body = JSON.parse(event.body);
            console.log('Parsed body:', body);
        } catch (error) {
            console.error('Error parsing event body:', error);
            return {
                statusCode: 400,
                body: JSON.stringify({ message: 'Invalid event body.' }),
            };
        }

        // Handle Slack's URL verification challenge
        if (body.type === 'url_verification') {
            console.log('Responding to URL verification challenge.');
            return {
                statusCode: 200,
                body: body.challenge,
            };
        }

        if (body.event) {
            console.log('Processing event:', body.event);

            // Ignore certain subtypes if necessary (e.g., channel_join)
            if (body.event.subtype && body.event.subtype !== 'bot_message') {
                console.log(`Ignoring event with subtype: ${body.event.subtype}`);
                return {
                    statusCode: 200,
                    body: JSON.stringify({ message: `Ignored event with subtype: ${body.event.subtype}` }),
                };
            }

            if (body.event.channel === "C09Q0SFCH") { // Replace with your channel ID
                console.log('Event is from the monitored channel.');

                const messageId = body.event.ts;
                const messageContent = body.event.text || (body.event.message && body.event.message.text);
                const userId = body.event.user; // Use the user ID from the payload
                const links = parseLinksFromBlocks(body.event.blocks);

                if (!messageContent) {
                    console.log('No message content found.');
                    return {
                        statusCode: 200,
                        body: JSON.stringify({ message: 'No message content found.' }),
                    };
                }

                console.log('Message content:', messageContent);

                // Attempt to parse a date from the message content
                const parsedDate = extractDateFromMessage(messageContent) || null;

                // Save the Slack message to Airtable
                try {
                    console.log('Saving to Airtable...');
                    const record = await base(TABLE_NAME).create([
                        {
                            fields: {
                                'Message ID': messageId,
                                'Message': messageContent,
                                'Author': userId,
                                'Date': parsedDate, // Store "Not specified" if no date found
                                'RSVP URL': links.join(', '),
                            }
                        }
                    ]);
                    console.log('Record saved:', record);

                    return {
                        statusCode: 200,
                        body: JSON.stringify({ message: 'Slack message saved to Airtable!' }),
                    };
                } catch (error) {
                    console.error('Error saving message to Airtable:', error.message, 'Code:', error.code);
                    return {
                        statusCode: 500,
                        body: JSON.stringify({ message: 'Error saving message to Airtable.', error: error.message }),
                    };
                }
            } else {
                console.log('Event not from the monitored channel.');
            }
        } else {
            console.log('No event found in the body.');
        }

        return {
            statusCode: 200,
            body: JSON.stringify({ message: 'Event not processed.' }),
        };
    }

    return {
        statusCode: 405,
        body: JSON.stringify({ message: 'Method Not Allowed' }),
    };
};

// Helper function to extract a date from message content
function extractDateFromMessage(messageContent) {
    const dateRegex = /(\d{4}-\d{2}-\d{2})|(\d{2}\/\d{2}\/\d{4})|(\d{2}-\d{2}-\d{4})/;
    const match = messageContent.match(dateRegex);

    if (match) {
        const dateStr = match[0];
        const parsedDate = new Date(dateStr);

        if (!isNaN(parsedDate)) {
            console.log('Parsed date:', parsedDate);
            return parsedDate.toISOString().split('T')[0]; // Return ISO date string (YYYY-MM-DD)
        }
    }

    console.log('No valid date found in message.');
    return null; // Return null if no valid date is found
}

// Helper function for parsing links from the blocks section
function parseLinksFromBlocks(blocks) {
    const links = [];
    if (!blocks || !Array.isArray(blocks)) {
        console.log('No blocks found or blocks is not an array.');
        return links;
    }

    blocks.forEach(block => {
        if (block.type === 'rich_text' && block.elements && Array.isArray(block.elements)) {
            block.elements.forEach(element => {
                if (element.elements && Array.isArray(element.elements)) {
                    element.elements.forEach(subElement => {
                        if (subElement.type === 'link') {
                            links.push(subElement.url);
                        }
                    });
                }
            });
        }
    });

    return links;
}