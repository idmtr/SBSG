# JavaScript Files

## slackJobHandler.js
**Purpose:** Handles incoming Slack events related to job postings.

**Functionality:**
- Acts as a Netlify Function to process HTTP requests from Slack.
- Verifies Slack's URL challenge during the initial setup.
- Parses and stores job postings from Slack messages into Airtable.
- Extracts relevant data such as message content, author, and attachments.

## getJobListings.js
#### For `/jobs` page & endpoint API.
**Purpose:** Fetches and serves job listings from Airtable.

**Functionality:**
- Retrieves job summaries from the JobSummaries table in Airtable.
- Parses the job data and formats it for the frontend.
- Provides endpoints to serve all jobs or a limited number for the front page.
- Separates featured jobs from regular listings for prioritized display.

## jobs-from-archive.js
**Purpose:** Processes archived job data from JSON files and imports it into Airtable.

**Functionality:**
- Reads JSON files containing archived Slack messages.
- Extracts job-related information like message content, author, email, and links.
- Converts Slack timestamps to readable dates.
- Creates new records in the Jobs table in Airtable with the extracted data.

## processJobs.js
**Purpose:** Analyzes unprocessed job posts and creates structured summaries.

**Functionality:**
- Fetches unprocessed job records from the Jobs table in Airtable.
- Uses OpenAI's API to determine if a message is a job ad and generates a structured summary.
- Stores the structured job summaries in the JobSummaries table.
- Marks the original job records as processed to prevent reprocessing.

## slackEventsHandler.js
**Purpose:** Handles Slack events related to event promotions.

**Functionality:**
- Acts as a Netlify Function to process event-related messages from Slack.
- Verifies Slack's URL challenge during setup.
- Uses OpenAI's API to analyze messages for event information.
- Extracts event details, such as summary and RSVP links.
- Saves event summaries to the Events table in Airtable.

## Additional Information

### Technologies Used:
- **Frontend:** SCSS, Bootstrap, Webpack.
- **Backend:** Node.js, Netlify Functions, Airtable API, Slack Events API.
- **APIs:** OpenAI API for content analysis and summarization.

### Services Integrated:
- **Airtable:** Serves as the database for storing jobs and events.
- **Slack:** Source of job postings and event messages.
- **Netlify:** Hosts the website and handles serverless function deployment.

### Setup and Deployment:
1. Clone the repository and install dependencies using `npm install`.
2. Use `npm run dev` to start the development server with hot reloading.
3. Deploy to Netlify, which uses `npm run prod` as the build command as specified in `netlify.toml`.

This project streamlines the process of collecting job postings and event information from Slack, processes them using AI, and presents them on a web platform, enhancing accessibility and user engagement.