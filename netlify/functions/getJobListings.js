const Airtable = require('airtable');

// Set up Airtable credentials
const AIRTABLE_API_KEY = 'pather5TbwcXeMHw3.46a6ec72bfd3670258f37efe05d6a9f580ee793dd3174d2575cfce69e2c63265';
const BASE_ID = 'appQ4nReiJhbgxsNL';
const JOB_SUMMARY_TABLE = 'JobSummaries'; // Table where structured summaries will be saved

// Initialize Airtable client
const base = new Airtable({ apiKey: AIRTABLE_API_KEY }).base(BASE_ID);

exports.handler = async (event, context) => {
    try {
        // Fetch jobs from Airtable
        const allJobs = await base(JOB_SUMMARY_TABLE).select({
            fields: ['Job Schema (JSON)', 'Posted On', 'Message ID', 'Featured'],
            maxRecords: 100,
            sort: [{ field: 'Posted On', direction: 'desc' }]
        }).all();

        console.log('Retrieved records:', allJobs); // Log the raw records

        // Map the records to extract relevant job data
        const jobList = allJobs.map(record => {
            let jobData = record.get('Job Schema (JSON)');
            const messageId = record.get('Message ID'); // Get the Message ID
            const postedOn = record.get('Posted On'); // Get the 'Posted On' date
            const isFeatured = record.get('Featured'); // Check if the job is featured

            if (jobData) {
                console.log('Raw job data:', jobData); // Log the raw data for debugging

                // Remove any surrounding code block markers
                jobData = jobData.replace(/```json\s*|```/g, '').trim();

                try {
                    const parsedJobData = JSON.parse(jobData);

                    // Ensure 'Job Type' is always a string
                    parsedJobData['Job Type'] = typeof parsedJobData['Job Type'] === 'string'
                      ? parsedJobData['Job Type']
                      : ''; // Default to an empty string

                    // Attach other fields
                    parsedJobData['Message ID'] = messageId || null;
                    parsedJobData['Posted On'] = postedOn
                      ? new Date(postedOn).toISOString().split('T')[0]
                      : 'Not specified';
                    parsedJobData['Featured'] = isFeatured || false;

                    // Calculate Days Left
                    if (postedOn) {
                      const postedOnDate = new Date(postedOn);
                      const expiryDate = new Date(postedOnDate);
                      expiryDate.setDate(expiryDate.getDate() + 31);
                      const daysLeft = Math.ceil((expiryDate - new Date()) / (1000 * 60 * 60 * 24));
                      parsedJobData['Days Left'] = daysLeft > 0 ? daysLeft : 0;
                    } else {
                      parsedJobData['Days Left'] = 'Not specified';
                    }
                    return parsedJobData;
                  } catch (parseError) {
                    console.error('Error parsing job data:', parseError.message);
                    return null;
                  }

            }
            return null;
        }).filter(job => job !== null);

        console.log('Processed job list:', jobList); // Log the processed job list

        // Filter jobs to include only those that are not expired (within 31 days of 'Posted On' date)
        const filteredJobList = jobList.filter(job => {
            const postedOnStr = job['Posted On'];
            const postedOnDate = new Date(postedOnStr);

            if (isNaN(postedOnDate)) {
                // Exclude jobs with invalid or unspecified 'Posted On' date
                return false;
            }

            // Calculate expiration date (31 days after 'Posted On' date)
            const expiryDate = new Date(postedOnDate);
            expiryDate.setDate(expiryDate.getDate() + 31);

            // Include the job if it's not expired
            return expiryDate >= new Date();
        });

        // For front page jobs, take the first 3 jobs from the filtered list
        const frontPageJobs = filteredJobList.slice(0, 3);

        if (event.path.includes('/front-page-jobs')) {
            // Return only 3 jobs for the front page
            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(frontPageJobs)
            };
        } else {
            // Return all jobs for the `/jobs` page
            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(filteredJobList)
            };
        }
    } catch (error) {
        console.error('Error fetching job listings from Airtable:', error);
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Error fetching job listings.' })
        };
    }
};
