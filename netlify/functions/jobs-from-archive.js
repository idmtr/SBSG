const fs = require('fs');
const path = require('path');
const Airtable = require('airtable');

// Airtable setup
const API_KEY = 'pather5TbwcXeMHw3.46a6ec72bfd3670258f37efe05d6a9f580ee793dd3174d2575cfce69e2c63265';
const BASE_ID = 'appQ4nReiJhbgxsNL';
const TABLE_NAME = 'Jobs';

const base = new Airtable({ apiKey: API_KEY }).base(BASE_ID);

// Directory containing your JSON files
const directoryPath = '../jobdata/';  // Replace with your directory path


// Helper function for parsing links from the blocks section
function parseLinksFromBlocks(blocks) {
    const links = [];
    if (!blocks || !Array.isArray(blocks)) {
        console.log('No blocks found or blocks is not an array.');
        return links;
    }

    blocks.forEach(block => {
        if (block.type === 'rich_text' && block.elements && Array.isArray(block.elements)) {
            block.elements.forEach(element => {
                if (element.elements && Array.isArray(element.elements)) {
                    element.elements.forEach(subElement => {
                        if (subElement.type === 'link') {
                            links.push(subElement.url);
                        }
                    });
                }
            });
        }
    });

    return links;
}

// Function to parse and process JSON files
const processFiles = async () => {
    try {
        const files = fs.readdirSync(directoryPath);
        const jsonFiles = files.filter(file => file.endsWith('.json'));

        for (const file of jsonFiles) {
            const filePath = path.join(directoryPath, file);
            const fileContent = fs.readFileSync(filePath, 'utf-8');
            const messages = JSON.parse(fileContent);

            console.log(`Processing file: ${file}`);

            for (const message of messages) {
                if (message.type === 'message' && !message.subtype) {
                    const messageId = message.ts;
                    const messageContent = message.text || 'No content available';
                    const userName = message.user_profile ? message.user_profile.real_name : 'Unknown User';

                    // Extract email from the message content
                    const emailRegex = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/i;
                    const emailMatch = messageContent.match(emailRegex);
                    const contactEmail = emailMatch ? emailMatch[0] : 'No email found';

                    // Extract links from the blocks
                    const links = parseLinksFromBlocks(message.blocks);

                    // Convert Slack timestamp to date format: YYYY-MM-DD
                    const date = new Date(parseFloat(message.ts) * 1000).toISOString().split('T')[0];

                    console.log(`Formatted date for message ID ${messageId}: ${date}`);
                    console.log(`Extracted email: ${contactEmail}`);
                    console.log(`Extracted links: ${links.join(', ')}`);

                    // Add the message to Airtable
                    try {
                        const record = await base(TABLE_NAME).create([
                            {
                                fields: {
                                    'Message ID': messageId,
                                    'Message': messageContent,
                                    'Author': userName,
                                    'Email': contactEmail,
                                    'Links': links.join(', '),
                                    'Posted On': date,
                                    'Tags': []  // Modify as needed
                                }
                            }
                        ]);
                        console.log(`Record added for message ID: ${messageId}`);
                    } catch (error) {
                        console.error(`Error adding message ID ${messageId} to Airtable:`, error);
                    }
                }
            }
        }
        console.log('All files processed.');
    } catch (error) {
        console.error('Error reading files:', error);
    }
};

// Run the function
processFiles();