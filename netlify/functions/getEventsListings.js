const Airtable = require('airtable');

// Set up Airtable credentials
const AIRTABLE_API_KEY = 'pather5TbwcXeMHw3.46a6ec72bfd3670258f37efe05d6a9f580ee793dd3174d2575cfce69e2c63265';
const BASE_ID = 'appQ4nReiJhbgxsNL';
const EVENTS_TABLE = 'Events'; // Table where event data is stored

// Initialize Airtable client
const base = new Airtable({ apiKey: AIRTABLE_API_KEY }).base(BASE_ID);

exports.handler = async (event, context) => {
    try {
        // Fetch events from Airtable
        const allEvents = await base(EVENTS_TABLE).select({
            fields: ['Message ID', 'Message', 'RSVP URL', 'Date', 'Featured'],
            maxRecords: 100,
            sort: [{ field: 'Date', direction: 'asc' }] // Sort events by date (oldest first)
        }).all();

        console.log('Retrieved records:', allEvents); // Log raw records for debugging

        // Map the records to extract relevant event data
        const eventList = allEvents.map(record => {
            const messageId = record.get('Message ID') || null; // Get the Message ID
            const message = record.get('Message') || null;
            const rsvpUrl = record.get('RSVP URL') || 'Not provided'; // Get the RSVP URL
            const eventDate = record.get('Date') || null;
            const isFeatured = record.get('Featured') || false; // Check if the event is featured

            // Calculate if the event is upcoming or expired
            let daysLeft = null;
            if (eventDate) {
                const eventDateObj = new Date(eventDate);
                const today = new Date();
                const timeDiff = eventDateObj - today;
                daysLeft = Math.ceil(timeDiff / (1000 * 60 * 60 * 24));
                if (daysLeft < 0) daysLeft = 0;
            }

            return {
                'Message ID': messageId,
                'Message': message,
                'RSVP URL': rsvpUrl,
                Date: eventDate,
                Featured: isFeatured,
                'Days Left': daysLeft
            };
        });

        console.log('Processed event list:', eventList); // Log the processed event list

        // Include events with 'Days Left' as null or greater than 0
        const upcomingEvents = eventList.filter(event => {
            return event['Days Left'] == null || event['Days Left'] > 0;
        });

        // Handle front page or full event list request
        if (event.path.includes('/front-page-events')) {
            // For front page, return only 3 featured events or the next 3 upcoming events
            const featuredEvents = upcomingEvents.filter(event => event.Featured);
            const frontPageEvents = featuredEvents.length > 0
                ? featuredEvents.slice(0, 3)
                : upcomingEvents.slice(0, 3);

            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(frontPageEvents)
            };
        } else {
            // Return all upcoming events
            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(upcomingEvents)
            };
        }
    } catch (error) {
        console.error('Error fetching event listings from Airtable:', error);
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Error fetching event listings.' })
        };
    }
};