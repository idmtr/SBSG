const Airtable = require('airtable');
const fetch = require('node-fetch');

// Set up OpenAI and Airtable credentials
const OPENAI_API_KEY = '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';
const AIRTABLE_API_KEY = 'pather5TbwcXeMHw3.46a6ec72bfd3670258f37efe05d6a9f580ee793dd3174d2575cfce69e2c63265';
const BASE_ID = 'appQ4nReiJhbgxsNL';
const JOBS_TABLE = 'Jobs'; // Table where job data from Slack is stored
const JOB_SUMMARY_TABLE = 'JobSummaries'; // Table where structured summaries will be saved

// Initialize Airtable client
const base = new Airtable({ apiKey: AIRTABLE_API_KEY }).base(BASE_ID);

// Helper function to call OpenAI API using `fetch`
const getJobSummary = async (jobData) => {
    const prompt = `Determine if the following text is a job advertisement. If it is a job ad, format it as a structured summary for Google's job schema, with sections like 'Title', 'Company', 'Location', 'Job Type', 'Description', 'Responsibilities', and 'Qualifications'. If it is not a job ad, respond with "NOT A JOB AD".

    Job Posting: ${jobData}

    Output only the structured summary in JSON format or the response "NOT A JOB AD".`;

    try {
        const response = await fetch('https://api.openai.com/v1/chat/completions', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${OPENAI_API_KEY}`
            },
            body: JSON.stringify({
                model: 'gpt-4o-mini',
                messages: [
                    { role: 'system', content: 'You are a helpful assistant that formats job postings into structured summaries.' },
                    { role: 'user', content: prompt }
                ],
                max_tokens: 500,
                temperature: 0.7
            })
        });

        const data = await response.json();
        if (data.choices && data.choices.length > 0) {
            const result = data.choices[0].message.content.trim();
            if (result === "NOT A JOB AD") {
                console.log('The message was not recognized as a job ad.');
                return null; // Skip processing if it's not a job ad
            }
            return result; // Return structured summary if it's a job ad
        }
        console.error('No valid response from OpenAI:', data);
        return null;
    } catch (error) {
        console.error('Error calling OpenAI API:', error);
        return null;
    }
};

// Function to fetch job data from the `Jobs` table and process it
const fetchAndProcessJobs = async () => {
    try {
        const records = await base(JOBS_TABLE).select({
            fields: ['Message', 'Posted On', 'Message ID', 'To Process'],
            filterByFormula: "AND(NOT({Processed}), {Message} != '', {To Process})" // Only fetch unprocessed jobs marked for processing
        }).all();

        if (records.length === 0) {
            console.log('No new job records to process.');
            return;
        }

        for (const record of records) {
            const jobId = record.get('Message ID');
            const messageContent = record.get('Message');
            const postedOn = record.get('Posted On');

            console.log('Processing job:', jobId);

            // Call OpenAI API for each job
            const summary = await getJobSummary(messageContent);
            if (summary) {
                console.log('Generated Summary:', summary);

                // Save the summary to `JobSummaries` table
                try {
                    await base(JOB_SUMMARY_TABLE).create([
                        {
                            fields: {
                                'Job Schema (JSON)': summary,
                                'Posted On': postedOn,
                                'Message ID': jobId // Link the job with the original `Jobs` record
                            }
                        }
                    ]);
                    console.log(`Summary saved for job ID: ${jobId}`);

                    // Mark the original job record as processed
                    await base(JOBS_TABLE).update(record.id, {
                        'Processed': true
                    });
                } catch (error) {
                    console.error(`Error saving summary for job ID ${jobId} to Airtable:`, error);
                }
            } else {
                console.log('Skipped processing for job:', jobId);
            }
        }

        console.log('All jobs processed.');
    } catch (error) {
        console.error('Error fetching job data from Airtable:', error);
    }
};

// Run the function to fetch and process job data
fetchAndProcessJobs();
