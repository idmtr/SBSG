const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
  entry: { main: "./views/index.js" },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[contenthash].js",
  },
  devServer: {
    static: {
      directory: path.resolve(__dirname, "dist"),
    },
    port: 7700,
    open: true, // Opens browser automatically
    historyApiFallback: true, // Handles SPA routing
  },
  optimization: {
    minimizer: [new CssMinimizerPlugin()],
  },
  module: {
    rules: [
      {
        test: /\.pug$/,
        use: ["pug-loader"],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.(scss|css)$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          "file-loader",
          {
            loader: "image-webpack-loader",
            options: {
              bypassOnDebug: true,
              disable: true,
            },
          },
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "fonts/",
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        { from: './assets/img', to: './assets/img' },
      { from: 'favicon.ico', to: 'favicon.ico' },
      { from: 'favicon-16x16.png', to: 'favicon-16x16.png' },
      { from: 'favicon-32x32.png', to: 'favicon-32x32.png' },
      { from: 'favicon-96x96.png', to: 'favicon-96x96.png' },
      { from: 'mstile-150x150.png', to: 'mstile-150x150.png' },
      { from: 'apple-icon-180x180.png', to: 'apple-icon-180x180.png' },
      { from: 'android-icon-192x192.png', to: 'android-icon-192x192.png' },
      { from: 'browserconfig.xml', to: 'browserconfig.xml' },
      { from: 'sitemap.xml', to: 'sitemap.xml' },
      { from: 'manifest.json', to: 'manifest.json' },
      { from: '_redirects', to: '_redirects', toType: 'file' }
      ],
    }),
    new MiniCssExtractPlugin({
      filename: '[contenthash].css'
    }),
    new HtmlWebpackPlugin({
      template: './views/home/index.pug',
      filename: 'index.html'
    }),
    new HtmlWebpackPlugin({
      template: "./views/bulletin/index.pug",
      filename: "./bulletin/index.html",
      page: "bulletin"
    }),
    new HtmlWebpackPlugin({
      template: "./views/interview-berlin-senate-economic-affairs-startups/index.pug",
      filename: "./interview-berlin-senate-economic-affairs-startups/index.html",
      page: "interview-berlin-senate-economic-affairs-startups"
    }),
    new HtmlWebpackPlugin({
      template: "./views/access/index.pug",
      filename: "./access/index.html",
      page: "access"
    }),
    new HtmlWebpackPlugin({
      template: "./views/welcome/index.pug",
      filename: "./welcome/index.html",
      page: "welcome"
    }),
    new HtmlWebpackPlugin({
      inject: true,
      hash: true,
      template: "./views/speakers/index.pug",
      filename: "./speakers/index.html",
      page: "speakers"
    }),
    new HtmlWebpackPlugin({
      template: "./views/jobs/index.pug",
      filename: "./jobs/index.html",
      page: "jobs"
    }),
    new HtmlWebpackPlugin({
      template: "./views/events/index.pug",
      filename: "./events/index.html",
      page: "events"
    }),
    new CleanWebpackPlugin()
  ],
};
